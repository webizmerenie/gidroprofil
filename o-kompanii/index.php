<?
define("ABOUT", "Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?><article>
<h3>ООО «Гидропрофиль»</h3>

<p>
<b>Год создания компании — 2009.<br>
Основное направление деятельности — поставка комплектующих для водоподготовки от
ведущих мировых производителей.
</b>
</p>

<p>Основные товарные группы:
<ul>
<li style="padding-left: 10px">OБРАТНООСМОТИЧЕСКИЕ И НАНОФИЛЬТРАЦИОННЫЕ МЕМБРАННЫЕ ЭЛЕМЕНТЫ</li>
<li style="padding-left: 10px">НАПОРНЫЕ КОРПУСА ДЛЯ МЕМБРАННЫХ ЭЛЕМЕНТОВ</li>
<li style="padding-left: 10px">ЗАЖИМЫ ДЛЯ ТРУБ ИЗ ЧУГУНА И НЕРЖАВЕЮЩЕЙ СТАЛИ</li>
<li style="padding-left: 10px">УЛЬТРАФИЛЬТРАЦИОННЫЕ МОДУЛИ</li>
<li style="padding-left: 10px">СТЕКЛОВОЛОКОННЫЕ КОРПУСА ЗАСЫПНЫХ ФИЛЬТРОВ</li>
<li style="padding-left: 10px">МУЛЬТИПАТРОННЫЕ И МЕШОЧНЫЕ ФИЛЬТРЫ</li>
<li style="padding-left: 10px">ФИЛЬТРОЭЛЕМЕНТЫ</li>
<li style="padding-left: 10px">РЕАГЕНТЫ</li>
</ul>
</p>

<p>Ключевые партнеры:</p>
<p>GE Water & Process Technologies (США)  /  UAB TIVE (ЛИТВА)  /  TORAY INDUSTRIES, INC. (ЯПОНИЯ)  /  VONTRON MEMBRANE TECHNOLOGY Co. (КИТАЙ)  /  ROPV Industry Development Center (КИТАЙ)  /  FIRSTLINE ENVIRONMENT TECHNOLOGY CO. LTD. (КИТАЙ)  /  PASS FLUID EQUIPMENT CO., LTD (КИТАЙ)  /  INGE GmbH (ГЕРМАНИЯ)
<!-- <p><img src="/upload/about.jpg"> -->

</article>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>