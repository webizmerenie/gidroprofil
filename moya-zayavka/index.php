<?
define("APPLICATION", "Y");
define("NO_CONTAINER", "Y");
define("NO_TITLE", "Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Моя заявка");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"breadcrumb",
	array(
		"COMPONENT_TEMPLATE" => "breadcrumb",
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"intec:catalog.basket", 
	"basket", 
	array(
		"ADMIN_MAIL" => ".",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"EVENT" => "SALE_ORDER",
		"EVENT_ADMIN" => "SALE_ORDER_ADMIN",
		"PROPERTY_CODE" => array(
		),
		"THANK_URL" => "/basket/thank_you.php",
		"UE" => "руб.",
		"COMPONENT_TEMPLATE" => "basket",
		"COMPOSITE_FRAME_MODE" => "N",
		"COMPOSITE_FRAME_TYPE" => "DYNAMIC_WITH_STUB"
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
