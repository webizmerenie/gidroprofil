###
* Scripts show/hide tooltip text
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

module.exports.init = ->
	$btn = $ '.icon-tooltip'

	$(document).on('mouseover', $btn.selector, ->
		$text = $(@).parent().find('.toolpti-text')
		$text.fadeIn(300)
	)

	$(document).on('mouseout', $btn.selector, ->
		$text = $(@).parent().find('.toolpti-text')
		$text.fadeOut(300)
	)
