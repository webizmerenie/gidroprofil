###
* Scripts from toggle height text
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

module.exports.init = ->
	$main = $ '.seo-text'
	$text = $main.find '.text'

	$text.click ->
		$(@).toggleClass 'show'
