###
* Function add basket
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

module.exports = (id, count) ->
	$smallBasket = $ '.navbar-nav li .counts'
	$preloader = $ '.preloader'
	$modal = $('.catalog').find '.modal-window'

	do $preloader.show

	$.post '/ajax/add_basket.php', {id, count}, (data) ->
		do $preloader.hide
		do $smallBasket.show if($smallBasket.is ':hidden')
		$modal.fadeIn(300)
		$smallBasket.text data.TOTAL_COUNT
