###
* Scripts from close button
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

module.exports.init = ->
	$(document).on('click', '.close', (e) ->
		target = $(@).data 'target'

		do e.preventDefault
		$(target).fadeOut(300)
		$('.sucsess-msg:visible').fadeOut(300)
	)
