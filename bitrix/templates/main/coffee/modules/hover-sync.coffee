###
* Scripts synchron gover two elements
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

module.exports = (nameHover, nameTarget) ->
	nameHover.hover(
		->
			$name = $(@).data 'name'
			if($name != '')
				$target = nameTarget.find('[data-name='+$name+']').parent()

				$target.toggleClass 'hover'
	)
