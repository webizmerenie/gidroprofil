###
* Scripts for forms
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

module.exports.init = ->
	$formClass = $ '.form'
	$form = $formClass.find 'form'
	$errorMsg = $formClass.find '.error-msg'
	$inputGroup = $form.find '.input-group'
	$filesBody = $form.find '.files'
	$fileGroup = $filesBody.find '.file-group'

	focusInput = ->
		$formElements = $inputGroup.find '*'

		$formElements.focus(->
			$(@).parent().addClass 'active'
			).blur(->
				$(@).parent().removeClass 'active'
				)

	addFiles = ->
		$add = $filesBody.find '.add'

		$add.on 'click', (e) ->
			$fileGroupVisible = $filesBody.find '.file-group:visible'

			do e.preventDefault

			do $(@).hide if $fileGroupVisible.length == 2

			length = $fileGroupVisible.length + 1
			$("#{$fileGroup.selector}:nth-child(#{length})")
				.fadeIn()
				.css("display": "inline-block")

	removeFiles = ->
		$inputs = $fileGroup.find 'input'

		$inputs.each(->
			$input = $(@)
			$deleteLink = $input.parent().find '.delete'
			$btn = $input.parent().find '.btn'

			if($input.val() != '')
				$deleteLink.fadeIn().css('display', 'block')
			else
				do $deleteLink.fadeOut

			$deleteLink.click( (e) ->
				do e.preventDefault

				$input.val ''
				do $(@).fadeOut
				$btn.removeClass 'error'
				$btn.text 'Прикрепить файл'
			)
		)

	validateFiles = (action) ->
		$inputs = $fileGroup.find 'input'
		arrExt = ['png', 'jpg', 'jpeg', 'gif', 'tif', 'xls',
			'doc', 'docx', 'odt', 'pdf']
		error = false
		sizes = []
		exts = []
		totalSize = 0

		if action == 'change'
			$inputs.change ->
				$but = $(@).parent().find 'label'
				$valInp = $(@).val()
				ext = $valInp.split('.').pop().toLowerCase()

				do removeFiles
				$valInp = "#{$valInp.slice(0, 13)}..." if $valInp.length > 15

				if($valInp != "")
					$but.text($valInp)

					if($.inArray(ext, arrExt) == -1)
						$but.addClass 'error'
						$errorMsg.text('Неверный формат файла')
					else
						$but.removeClass 'error'

		if action == 'submit'
			$inputs.each ->
				$valInp = $(@).val()
				ext = $valInp.split('.').pop().toLowerCase()

				if($valInp != "")
					size = @.files[0].size
					sizes.push(size)
					exts.push(ext)

			for ext in exts
				if($.inArray(ext, arrExt) == -1)
					error = true
					$errorMsg.text('Неверный формат файла')

			for size in sizes
				totalSize = totalSize + size
			if(totalSize > 10485760)
				error = true
				$errorMsg.text('Размер всех файлов не должен привышать 10MB')
		return error

	validateForm = (form) ->
		error = false
		$input = $(form).find '.require'

		$input.each ->
			$(@).val '' if $(@).val().replace(/\s/g, '') == ''
			if $(@).val() == '' or $(@).val() == 'Ваш регион'
				error = true
				$(@).parent().addClass 'error'
				$errorMsg.text('Необходимо запонить все поля')
			else
				$(@).parent().removeClass 'error'

		return error

	$form.submit (event) ->
		do event.preventDefault
		errorFiles = validateFiles('submit')
		$sucsessMsg = $(@).parent().find '.sucsess-msg'
		$preloader = $ '.preloader'

		error = validateForm $(@)

		postData = new FormData($(@)[0])

		if error or errorFiles
			$errorMsg.css
				"visibility": "visible"
				"opacity": 1
		else
			do $preloader.show

			$.ajax(
				url: "/ajax/send_form.php",
				type: "POST",
				data: postData,
				processData: false,
				contentType: false,
				success: ->
					do $preloader.hide
					$form.trigger 'reset'
					$errorMsg.css
						"visibility": "hidden"
						"opacity": 0
					do $sucsessMsg.fadeIn
					do $form[0].reset
			)
		return false;

	do focusInput
	validateFiles 'change'
	do addFiles
