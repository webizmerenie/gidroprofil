###
* Main scripts file
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

bootstrap = require 'bootstrap'
mainPage = require './pages/main'
contacts = require './pages/contacts'
form = require './modules/form'
catalogSchemsPage = require './pages/catalog-schems'
tooltip = require './modules/tooltip'
close = require './modules/close'
catalogItems = require './pages/catalog-items'
catalogElement = require './pages/catalog-element'
application = require './pages/application'
toggleText = require './modules/text-toggle'
catalogFilter = require './pages/catalog-filter'

$ ->
	bootstrap
	do mainPage.init
	do contacts.init
	do form.init
	do catalogSchemsPage.init
	do tooltip.init
	do close.init
	do catalogItems.init
	do catalogElement.init
	do application.init
	do toggleText.init
	do catalogFilter.init

	$("header #title-search form").submit (e) ->
		do e.preventDefault
