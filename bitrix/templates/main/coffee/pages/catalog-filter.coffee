###
* Scripts from filters catalog
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

module.exports.init = ->
	$main = $ '.filter'
	$link = $main.find '.toggle-filter'
	$filter = $main.find '.bx_filter_section'
	$inputs = $filter.find 'input'
	$arrHref = window.location.pathname.split '/'

	$link.click( (e) ->
		text = $(@).text()

		do e.preventDefault
		do $filter.slideToggle
		$main.toggleClass 'filter-none'

		if text == 'Открыть фильтр'
			$(@).text 'Скрыть фильтр'
		else
			$(@).text 'Открыть фильтр'
	)

	if $inputs.is(':checked') == true or $arrHref[4] == 'clear'
		$main.removeClass 'filter-none'
		$link.text 'Скрыть фильтр'
		do $filter.show

	if $inputs.is(':checked') == true
		window.scrollTo(0, $("#items").offset().top)
		
	if $arrHref[4] == 'clear'
		window.scrollTo(0, $main.offset().top)
