###
* Scripts from page catalog application
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

module.exports.init = ->
	$main = $ '.application'
	$listItem = $main.find '.application-table'
	$containTotal = $listItem.find '.total'
	$totalPrice = $containTotal.find '.total-price span'
	$totalCount = $containTotal.find '.total-count span'
	$navCounts = $ '.nav .counts'
	$input = $listItem.find '.count'
	$preloader = $ '.preloader'

	deleteItem = ->
		$deleteBtn = $listItem.find '.delete'

		$deleteBtn.click (e) ->
			$tr = $(@).closest 'tr'
			id = $(@).data 'id'

			do e.preventDefault
			do $preloader.show

			$.post '/ajax/remove_basket.php', {
				id: id
			}, (data) ->
				do $preloader.hide
				$tr.fadeOut '300', -> $(@).remove()
				$totalPrice.text(data.TOTAL_PRICE)
				$totalCount.text(data.TOTAL_COUNT)
				$navCounts.text(data.TOTAL_COUNT)
				do location.reload if $totalPrice.text() == "0"

	delay = do ->
		timer = 0
		(callback, ms) ->
			clearTimeout timer
			timer = setTimeout(callback, ms)

	updateCount = (priceSelector, id, count) ->
		do $preloader.show

		$.post '/ajax/update_count.php', {
			id: id,
			count: count
		}, (data) ->
			do $preloader.hide
			$totalPrice.text(data.TOTAL_PRICE)
			$totalCount.text(data.TOTAL_COUNT)
			$navCounts.text(data.TOTAL_COUNT)
			$(priceSelector).text(data.ITEM_PRICE)

	increaseCount = ->
		$add = $listItem.find '.add'

		$add.click (e) ->
			$container = $(@).closest('tr')
			$input = $container.find '.count'
			count = Number($input.val())
			id = $input.data 'id'
			$price = $container.find '.item-price'

			do e.preventDefault
			$input.val(count + 1)
			count = Number($input.val())

			updateCount($price, id, count)

	$input.on 'input', ->
		count = $(@).val()
		id = $(@).data 'id'
		$item = $(@).closest 'tr'
		$price = $item.find '.item-price'

		if count == "0" or count == ""
			$(@).val 1
			count = 1

		delay (->
			updateCount($price, id, count)
		), 1000


	do deleteItem
	do increaseCount
