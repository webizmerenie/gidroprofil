###
* Scripts from page catalog schems
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'
hoverSync = require '../modules/hover-sync'

module.exports.init = ->
	$main = $ '.catalog'
	$schems = $main.find '.schems'
	$imgBody = $schems.find '.elements'
	$imgItems = $imgBody.find 'img'
	$linksBody = $schems.find '.links'
	$linkItems = $linksBody.find 'a'

	#hoverSync($imgItems, $linksBody)
	#hoverSync($linkItems, $imgBody)
