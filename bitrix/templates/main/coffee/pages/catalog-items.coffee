###
* Scripts from page catalog schems
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

addBasket = require '../modules/add-basket'

module.exports.init = ->
	$main = $ '.list-items'
	$addBtn = $main.find 'input[name="add"]'
	$smallBasket = $ '.navbar-nav li .counts'

	do $smallBasket.show if($smallBasket.text() != '' and $smallBasket.text() != '0')

	$(document).on 'click', $addBtn.selector,  ->
		$tr = $(@).closest 'tr'
		id = $(@).data 'id'
		count = $tr
			.find 'input[name="count"]'
			.val()

		addBasket(id, count)
