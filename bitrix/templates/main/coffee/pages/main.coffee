###
* Scripts for main page
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'
hoverSync = require '../modules/hover-sync'

module.exports.init = ->
	$main = $ '.main'
	$modal = $main.find '.modal'
	$modalBody = $modal.find '.module-body'
	$imgsBody = $modalBody.find '.img .elements'
	$imgs = $imgsBody.find 'a img'
	$linksBody = $modalBody.find '.links ul'
	$links = $linksBody.find 'li a'

	$modal.on 'shown.bs.modal', ->
		$this = $(@)
		$container = $this.find '.container'
		$window = $(window)

		if($window.height() <= $container.height())
			$this.addClass 'absolute'
		else
			$this.removeClass 'absolute'

	hoverSync($imgs, $linksBody)
	hoverSync($links, $imgsBody)
