###
* Scripts from page catalog schems
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

addBasket = require '../modules/add-basket'

module.exports.init = ->
	$main = $ '.detail'
	$btnAddBasket = $main.find '.add-basket'

	$btnAddBasket.click ->
		id = $(@).data 'id'
		count = 1

		addBasket(id, count)
