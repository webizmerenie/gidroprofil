###
* Scripts for main page
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
###

$ = require 'jquery'

module.exports.init = ->
	$main = $ '.contacts'
	$links = $main.find 'li a'
	$maps = $main.find '.map'

	$($links[0]).addClass 'active'
	$($maps[0]).addClass 'active'

	$links.click (e) ->
		$this = $(@)
		target = $this.attr 'href'

		do e.preventDefault

		if !$this.hasClass 'active'
			$links.removeClass 'active'
			$maps.fadeOut().removeClass('active')

			$this.addClass 'active'
			$(target).fadeIn().addClass('active')
