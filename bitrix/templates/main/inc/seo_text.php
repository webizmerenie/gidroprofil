<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$arSelect = Array("NAME", "DETAIL_TEXT", "property_ATT_TITLE", "property_ATT_DESC");
$arFilter = Array(
	"IBLOCK_ID"=>$arParams['IBLOCK_ID'],
	"ACTIVE"=>"Y",
	"NAME"=>$arParams['ELEMENT_LINK']
);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement())
{
	$arResult = $ob->GetFields();
}
if($arResult["PROPERTY_ATT_DESC_VALUE"]["TEXT"]) {
	$APPLICATION->SetPageProperty('description', $arResult["PROPERTY_ATT_DESC_VALUE"]["TEXT"]);
}
if($arResult["PROPERTY_ATT_TITLE_VALUE"]) {
	$APPLICATION->SetPageProperty('title', $arResult['PROPERTY_ATT_TITLE_VALUE']);
}
?>
<?if($arResult['DETAIL_TEXT']){?>
	<section class="seo-text container">
		<h1>Дополнительная информация</h1>
		<div class="text"><?=$arResult['DETAIL_TEXT'];?></div>
	</section>
<?}?>
