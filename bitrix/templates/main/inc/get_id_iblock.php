<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$id_ib = CIBlockElement::GetList(
	array(),
	array('IBLOCK_CODE' => $_REQUEST['IBLOCK_ID']),
	false,
	array('nTopCount' => 1),
	array('IBLOCK_ID', 'IBLOCK_NAME', 'IBLOCK_CODE'))->Fetch();

$APPLICATION->AddChainItem($id_ib['IBLOCK_NAME'], "/katalog/".$id_ib['IBLOCK_CODE']."/");

$imgId = CFile::GetFileArray(CIBlock::GetArrayByID($id_ib['IBLOCK_ID'], "PICTURE"));

$img = CFile::ResizeImageGet(
	$imgId,
	array('width'=>914, 'height'=>624),
	BX_RESIZE_IMAGE_PROPORTIONAL,
	true,
	100
);

$id_ib['IMG'] = $img;

return $id_ib;
