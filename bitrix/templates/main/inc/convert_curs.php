<?
function convert($item, $arParams){
	if($item['CODE'] == 'USD')
		$item['CONVERT_PRICE'] = round($item['VALUE'] * $arParams["USD"]);
	if($item['CODE'] == 'EUR')
		$item['CONVERT_PRICE'] = round($item['VALUE'] * $arParams["EUR"]);
	if($item['CODE'] == 'RUB')
		$item['CONVERT_PRICE'] = round($item['VALUE']);

	return $item;
}
