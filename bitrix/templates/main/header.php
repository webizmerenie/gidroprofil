<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$tplPath = SITE_TEMPLATE_PATH;
$uri = $APPLICATION->GetCurUri();
global $tplPath;

$mainClass = "";

if(defined("MAIN"))
	$mainClass = "main";

if(defined("ABOUT"))
	$mainClass = "about";

if(defined('CONTACTS'))
	$mainClass = "contacts";

if(defined("CATALOG"))
	$mainClass = "catalog";

if(defined("APPLICATION"))
	$mainClass = "application";
?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<title><?$APPLICATION->ShowTitle();?></title>
		<meta charset="utf-8">
		<meta name="viewport" content-width="width=device-width">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<link rel="stylesheet" href="<?=$tplPath;?>/app/build.css">
		<script async src="<?=$tplPath?>/app/build.js"></script>
		<?$APPLICATION->ShowHead();?>
	</head>
	<body>
		<?$APPLICATION->ShowPanel();?>
		<div class="wrapper">
			<header>
				<div class="container">
					<div class="column">
						<?if($uri == '/'){?>
							<div class="logo"></div>
						<?}else{?>
							<a href="/" class="logo"></a>
						<?}?>
					</div>
					<div class="column">
						<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"header",
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "",
		"COMPONENT_TEMPLATE" => "header",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPOSITE_FRAME_MODE" => "N"
	),
	false
);?>
					</div>
					<div class="column">
						<?$APPLICATION->IncludeComponent(
	"bitrix:search.title",
	"header",
	array(
		"CATEGORY_0" => array(
			0 => "iblock_bytovaya_vodopodgotovka",
			1 => "iblock_prom_podgotovka",
		),
		"CATEGORY_0_TITLE" => "",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "header",
		"CONTAINER_ID" => "title-search",
		"INPUT_ID" => "title-search-input",
		"NUM_CATEGORIES" => "1",
		"ORDER" => "date",
		"PAGE" => "/search.php",
		"SHOW_INPUT" => "Y",
		"SHOW_OTHERS" => "N",
		"TOP_COUNT" => "10",
		"USE_LANGUAGE_GUESS" => "Y",
		"PRICE_CODE" => "",
		"PRICE_VAT_INCLUDE" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SHOW_PREVIEW" => "Y",
		"CATEGORY_0_main" => "",
		"CATEGORY_0_iblock_prom_podgotovka" => array(
			0 => "all",
		),
		"CATEGORY_0_iblock_bytovaya_vodopodgotovka" => array(
			0 => "all",
		)
	),
	false
);?>
					</div>
					<div class="column">
						<button id="ask-question" data-toggle="modal" data-target="#call-window" class="btn btn-default">Задать вопрос</button>
					</div>
				</div>
			</header>
			<main class="content <?=$mainClass;?>">
				<?if(!defined('NO_TITLE')){?>
					<div class="title">
						<h1><?$APPLICATION->ShowTitle(false);?></h1>
					</div>
				<?}?>
				<?if(!defined('NO_CONTAINER')){?>
					<div class="container">
				<?}?>
