<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$left = 0;
$top = 0;
$width = "20%";
?>
<a href="<?=$arResult['LIST_PAGE_URL'];?><?=$arResult['UF_IBLOCK_CODE'];?>/">
	<?foreach($arResult['ITEMS'] as $arItem){
		if($arItem['PROPERTIES']['LEFT_CATALOG']['VALUE'])
			$left = $arItem['PROPERTIES']['LEFT_CATALOG']['VALUE'];
		if($arItem['PROPERTIES']['TOP_CATALOG']['VALUE'])
			$top = $arItem['PROPERTIES']['TOP_CATALOG']['VALUE'];
		if($arItem['PROPERTIES']['WIDTH_CATALOG']['VALUE'])
			$width = $arItem['PROPERTIES']['WIDTH_CATALOG']['VALUE'];?>

		<?if($arItem['DETAIL_PICTURE']['SRC']){?>
			<img src="<?=$arItem['DETAIL_PICTURE']['SRC'];?>"
			data-name="<?=$arResult['CODE']?>"
			title="<?=$arResult['NAME'];?>"
			style="left: <?=$left;?>; top: <?=$top?>; width: <?=$width?>;">
		<?}?>
	<?}?>
</a>
