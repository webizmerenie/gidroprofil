<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult['ITEMS']){?>
	<table class="table" id="items">
		<thead>
			<tr>
				<th><span>Модель</span></th>
				<?foreach($arResult['ITEMS'][0]['PROPERTIES'] as $arPropHead){?>
					<?if($arPropHead['PROPERTY_TYPE'] == "S"){?>
						<th>
							<span><?=$arPropHead['NAME'];?></span>
						</th>
					<?}?>
				<?}?>
				<th class="price">
					<span>Цена, руб.</span>
						<div class="tooltip-custom">
							<button class="icon-tooltip"></button>
							<div class="toolpti-text">
								Предусмотрена гибкая система скидок от указанной цены
							</div>
						</div>
				</th>
				<th><span>Укажите количество</span></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?foreach($arResult['ITEMS'] as $arItem){?>
				<tr>
					<td>
						<a href="<?=$arItem['DETAIL_PAGE_URL'];?>">
							<?=$arItem['NAME'];?>
						</a>
					</td>
					<?foreach($arItem['PROPERTIES'] as $arProps){?>
						<?if($arProps['PROPERTY_TYPE'] == "S"){?>
							<td><?=$arProps['VALUE'];?></td>
						<?}?>
					<?}?>
					<td><?=$arItem['PRICE']['CONVERT_PRICE'];?></td>
					<td>
						<input type="text" value="1" maxlength="4" name="count">
					</td>
					<td>
						<input type="button" name="add" value="Добавить к заявке" class="btn" data-id="<?=$arItem['ID'];?>">
					</td>
				</tr>
			<?}?>
		</tbody>
	</table>
	<?if ($arParams["DISPLAY_BOTTOM_PAGER"]){
		echo $arResult["NAV_STRING"];
	}?>
	<div class="modal-window">
		<button class="close" data-target=".modal-window"></button>
		<div class="text">
			<h3>Ваш товар добавлен к заявке.</h3>
			<a href="/moya-zayavka/">Перейти в мою заявку</a>
			<a href="#close" class="close" data-target=".modal-window">Продолжить выбор</a>
		</div>
	</div>
<?}?>
