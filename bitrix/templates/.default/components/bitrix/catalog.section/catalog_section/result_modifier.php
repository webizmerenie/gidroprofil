<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/main/inc/convert_curs.php');

$res = CIBlock::GetByID($arResult['IBLOCK_ID'])->GetNext();

$arResult['IBLOCK_NAME'] = $res['NAME'];

foreach($arResult['ITEMS'] as $k => $arItem){
	$price = array();
	foreach($arItem['PROPERTIES'] as $arProp){
		if($arProp['PROPERTY_TYPE'] == "N" && !empty($arProp['VALUE'])){
			$price = $arProp;
		}
	}
	$arResult['ITEMS'][$k]['PRICE'] = convert($price, $arParams);
}
