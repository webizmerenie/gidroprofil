<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?if($arResult['ITEMS']){?>
	<section class="filter filter-none">
		<div class="container">
			<h3>Фильтр по параметрам</h3>
			<a class="toggle-filter" href="#toggle">Открыть фильтр</a>
		</div>
		<div class="bx_filter_section">
			<form class="container">
				<div class="bx_filter_items">
					<?foreach($arResult["ITEMS"] as $key => $arItem){?>
						<div class="bx_filter_item">
							<div class="bx_filter_parameters_box_title">
								<?=$arItem["NAME"]?>
							</div>
							<?foreach($arItem["VALUES"] as $val => $ar){?>
								<label data-role="label_<?=$ar["CONTROL_ID"]?>"
									class="bx_filter_param_label <?=$ar["DISABLED"] ? 'disabled': '' ?>"
									for="<?=$ar["CONTROL_ID"] ?>">
									<input
										type="checkbox"
										value="<?=$ar["HTML_VALUE"] ?>"
										name="<?=$ar["CONTROL_NAME"] ?>"
										id="<?=$ar["CONTROL_ID"] ?>"
										<?=$ar["CHECKED"]? 'checked="checked"': '' ?>
										<?=$ar["DISABLED"] ? 'disabled="disabled"': '' ?>
										onclick="smartFilter.click(this)"
									/>
									<span class="bx_filter_param_text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?></span>
								</label>
							<?}?>
						</div>
					<?}?>
					<a class="send btn bubl" href="<?=$arResult["FILTER_URL"]?>">Показать</a>
				</div>
			</form>
			<div id="modef">
				<a class="btn" href="<?=$arResult['SEF_DEL_FILTER_URL'];?>">Сбросить</a>
			</div>
		</div>
	</section>
	<script>
		var smartFilter= new JCSmartFilter('<?=CUtil::JSEscape($arResult["FORM_ACTION"])?>', 'horizontal');
	</script>
<?}?>
