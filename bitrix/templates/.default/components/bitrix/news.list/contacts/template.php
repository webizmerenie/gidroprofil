<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?if($arResult['ITEMS']){?>
	<div class="container">
		<h2>Гидропрофиль</h2>
		<ul>
			<?foreach($arResult['ITEMS'] as $arItem){?>
				<li><a href="#map<?=$arItem['ID'];?>"><?=$arItem['NAME'];?></a>
					<p>Тел.: <?=$arItem['PROPERTIES']['TELEPHONE']['VALUE'];?></p>
					<p>E-mail: <?=$arItem['PROPERTIES']['EMAIL']['VALUE'];?></p>
				</li>
			<?}?>
		</ul>
	</div>
	<?foreach($arResult['ITEMS'] as $arMap){?>
		<?if($arMap['PROPERTIES']['MAP']['VALUE']){?>
			<div class="map" id="map<?=$arMap['ID']?>">
				<?$APPLICATION->IncludeComponent(
					"bitrix:map.yandex.view",
					".default",
					array(
						"COMPONENT_TEMPLATE" => ".default",
						"CONTROLS" => array(
							0 => "ZOOM",
							1 => "TYPECONTROL",
						),
						"INIT_MAP_TYPE" => "PUBLIC",
						"MAP_DATA" => serialize(array("yandex_lat"=>$arMap["PROPERTIES"]["MAP"]["LAT"],"yandex_lon"=>$arMap["PROPERTIES"]["MAP"]["LON"],"yandex_scale"=>$arMap["PROPERTIES"]["MAP_SCALE"]["VALUE"],"PLACEMARKS"=>array(array("TEXT"=>$arMap["PROPERTIES"]["MAP_TEXT"]["VALUE"],"LAT"=>$arMap["PROPERTIES"]["MAP"]["LAT"],"LON"=>$arMap["PROPERTIES"]["MAP"]["LON"]),),)),
						"MAP_HEIGHT" => "605",
						"MAP_ID" => "yamap".$arMap["ID"],
						"MAP_WIDTH" => "100%",
						"OPTIONS" => array(
							0 => "ENABLE_DRAGGING",
						)
					),
					false
				);?>
			</div>
		<?}?>
	<?}?>
<?}?>
