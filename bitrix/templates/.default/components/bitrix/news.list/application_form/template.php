<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="applicataion-form form">
	<h3>Оформить заявку</h3>
	<form>
		<input type="hidden" name="type_form" value="application">
		<div class="input-group">
			<div class="input-group-addon">
				<label for="name">Ваше имя:</label>
			</div>
			<input type="text" name="name" id="name" class="require">
		</div>
		<div class="input-group">
			<div class="input-group-addon">
				<label for="tel">Телефон или e-mail:</label>
			</div>
			<input type="text" name="tel" id="tel" class="require">
		</div>
		<div class="input-group">
			<select name="region" class="require">
				<option selected hidden>Ваш регион</option>
				<?foreach($arResult['ITEMS'] as $item){?>
					<option value="<?=$item['ID'];?>"><?=$item['NAME'];?></option>
				<?}?>
			</select>
		</div>
		<div class="input-group">
			<textarea name="msg" placeholder="Дополнительная информация" class="require"></textarea>
		</div>
		<div class="error-msg">Необходимо запонить все поля</div>
		<div class="input-submit">
			<input type="submit" value="Отправить" class="btn">
		</div>
	</form>
	<div class="sucsess-msg">
		<div class="container">
			<h3>Ваш запрос отправлен</h3>
			<p>Спасибо за ваш вопрос. <br>
				Мы свяжемся с вами в течение
				2 часов.
			</p>
			<a class="btn" href="/katalog/">Перейти в каталог</a>
		</div>
	</div>
</section>
