<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arResult['ITEMS']){?>
	<section class="developments">
		<div class="title">
			<h1>События</h1>
		</div>
		<div class="container">
			<div id="developments-carousel" data-ride="carousel" data-interval="false" class="carousel slide">
				<div class="carousel-inner">
					<?foreach($arResult['ITEMS'] as $k => $arItem){
					$img = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'],
					array('width'=>260, 'height'=>260), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>

						<div class="item <?if($k == 0){?>active<?}?>">
							<div class="img">
								<img src="<?=$img['src'];?>">
							</div>
							<div class="text"><?=$arItem['PREVIEW_TEXT'];?></div>
						</div>
					<?}?>
				</div>
				<a href="#developments-carousel" data-slide="prev" class="control left-arrow"></a>
				<a href="#developments-carousel" data-slide="next" class="control right-arrow"></a>
			</div>
		</div>
	</section>
<?}?>
