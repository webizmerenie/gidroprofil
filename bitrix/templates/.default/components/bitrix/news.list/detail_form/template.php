<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="detail-form form">
	<h3>Техническая консультация</h3>
	<form>
		<input type="hidden" name="type_form" value="detail_page">
		<input type="hidden" name="item_name" value="<?=$arParams['ITEM_NAME'];?>">
		<div class="input-group">
			<div class="input-group-addon">
				<label for="name">Ваше имя:</label>
			</div>
			<input type="text" name="name" id="name" class="require">
		</div>
		<div class="input-group">
			<div class="input-group-addon">
				<label for="tel">Телефон или e-mail:</label>
			</div>
			<input type="text" name="tel" id="tel" class="require">
		</div>
		<div class="input-group">
			<select name="region" class="require">
				<option selected hidden>Ваш регион</option>
				<?foreach($arResult['ITEMS'] as $item){?>
					<option value="<?=$item['ID'];?>"><?=$item['NAME'];?></option>
				<?}?>
			</select>
		</div>
		<div class="input-group">
			<textarea name="msg" placeholder="Ваш вопрос" class="require"></textarea>
		</div>
		<div class="files">
			<div class="file-group">
				<label for="file0" class="btn">Прикрепить файл</label>
				<input type="file" id="file0" name="file0"><a href="#file0" title="Удалить файл" class="delete">удалить файл </a>
			</div>
			<div class="file-group">
				<label for="file1" class="btn">Прикрепить файл</label>
				<input type="file" id="file1" name="file1"><a href="#file1" title="Удалить файл" class="delete">удалить файл</a>
			</div>
			<div class="file-group">
				<label for="file2" class="btn">Прикрепить файл</label>
				<input type="file" id="file2" name="file2"><a href="#file2" title="Удалить файл" class="delete">удалить файл</a>
			</div>
			<button class="btn add">+</button>
		</div>
		<div class="error-msg">Необходимо запонить все поля</div>
		<div class="input-submit">
			<input type="submit" value="Отправить" class="btn">
		</div>
	</form>
	<div class="sucsess-msg">
		<div class="container">
			<h3>Ваш запрос отправлен</h3>
			<p>Спасибо за ваш вопрос. <br>
				Мы свяжемся с вами в течение
				2 часов.
			</p>
			<button class="close" data-target=".sucsess-msg"></button>
		</div>
	</div>
</section>
