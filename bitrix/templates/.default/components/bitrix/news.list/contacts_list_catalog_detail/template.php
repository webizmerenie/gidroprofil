<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="contacts-block">
	<h4>Контактная информация</h4>
	<ul>
		<?foreach($arResult['ITEMS'] as $arItem){?>
			<li>
				<p><b><?=$arItem['NAME'];?></b></p>
				<p>Тел.: <?=$arItem['PROPERTIES']['TELEPHONE']['VALUE'];?></p>
				<p>E-mail: <?=$arItem['PROPERTIES']['EMAIL']['VALUE'];?></p>
			</li>
		<?}?>
	</ul>
</div>
