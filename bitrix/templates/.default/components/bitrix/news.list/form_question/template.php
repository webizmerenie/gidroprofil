<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="modal fade" id="call-window" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Задать вопрос</h4>
			</div>
			<div class="modal-body form">
				<form>
					<input type="hidden" name="type_form" value="call_me">
					<div class="input-group">
						<div class="input-group-addon">
							<label for="name">Ваше имя:</label>
						</div>
						<input type="text" name="name" id="name" class="require"/>
					</div>
					<div class="input-group">
						<div class="input-group-addon">
							<label for="tel">Телефон или e-mail:</label>
						</div>
						<input type="text" name="tel" id="tel" class="require"/>
					</div>
					<div class="input-group">
						<select name="region" class="require">
							<option selected hidden>Ваш регион</option>
							<?foreach($arResult['ITEMS'] as $item){?>
								<option value="<?=$item['ID'];?>"><?=$item['NAME'];?></option>
							<?}?>
						</select>
					</div>
					<div class="input-group">
						<textarea name="msg" placeholder="Ваше сообщение" class="require"></textarea>
					</div>
					<div class="error-msg">Необходимо запонить все поля</div>
					<div class="input-submit">
						<input type="submit" value="Отправить" class="btn"/>
					</div>
				</form>
				<div class="sucsess-msg">
					<button data-dismiss="modal" class="close"></button>
					<div class="container">
						<h3>Ваш запрос отправлен</h3>
						<p>Спасибо за вашу зявку. </br>
							Мы свяжемся с вами в течение рабочего дня.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
