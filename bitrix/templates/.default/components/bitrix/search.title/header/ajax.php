<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arResult['CATEGORIES'][0]['ITEMS']){
	$count = count($arResult['CATEGORIES'][0]['ITEMS']) - 1;
	unset($arResult['CATEGORIES'][0]['ITEMS'][$count]);
	?>
	<ul>
		<?foreach($arResult['CATEGORIES'][0]['ITEMS'] as $arItem){?>
			<li>
				<a href="<?=$arItem['URL'];?>"><?=$arItem['NAME'];?></a>
			</li>
		<?}?>
	</ul>
<?}?>
