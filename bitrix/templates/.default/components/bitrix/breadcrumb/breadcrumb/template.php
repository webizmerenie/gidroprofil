<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

$strReturn .= '<section class="title"><div class="container"><ul class="breadcrumb">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	if(($itemSize - 1) != $index){
		$strReturn .= '<li><a href="'.$arResult[$index]['LINK'].'">'.$arResult[$index]['TITLE'].'</a></li>';
	}else{
		$strReturn .= '<li class="active">'.$arResult[$index]['TITLE'].'</li>';
	}
}

$strReturn .= '</ul></div></section>';

return $strReturn;
