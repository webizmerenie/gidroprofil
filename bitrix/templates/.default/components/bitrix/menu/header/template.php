<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<nav class="navbar navbar-default">
	<ul class="nav navbar-nav">
		<?foreach($arResult as $arItem){?>
			<li <?if($arItem['SELECTED']){ print "class='active'"; }?>>
				<a href="<?=$arItem['LINK']?>"><?=$arItem['TEXT'];?></a>
				<?if($arItem['PARAMS']['CART']){?>
					<?$APPLICATION->IncludeComponent(
						"intec:catalog.basket.small",
						"header",
						Array(
							"BASKET_URL" => "/moya-zayavka/",
							"COMPONENT_TEMPLATE" => ".default",
							"VALUTA" => "руб."
						)
					);?>
				<?}?>
			</li>
		<?}?>
	</ul>
</nav>
