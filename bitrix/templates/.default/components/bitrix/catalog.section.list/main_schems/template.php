<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?if($arResult['SECTIONS']){?>
	<section class="schema">
		<div class="container">
			<div class="row">
				<?foreach($arResult['SECTIONS'] as $arSection){
					$img = CFile::ResizeImageGet(
						$arSection['PICTURE'],
						array('width'=>599, 'height'=>488),
						BX_RESIZE_IMAGE_PROPORTIONAL,
						true,
						100
					);
					$imgHover = CFile::ResizeImageGet(
						$arSection['HOVER_PICTURE'],
						array('width'=>599, 'height'=>488),
						BX_RESIZE_IMAGE_PROPORTIONAL,
						true,
						100
					);?>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<a href="#popap1" data-toggle="modal" data-target="#popap<?=$arSection['ID'];?>">
								<p><?=$arSection['NAME'];?></p>
								<div class="img">
									<img src="<?=$img['src'];?>">
									<img src="<?=$imgHover['src'];?>" class="hover">
								</div>
							</a>
						</div>
				<?}?>
			</div>
		</div>
		<?foreach($arResult['SECTIONS'] as $arPop){
			$imgProp = CFile::ResizeImageGet(
				$arPop['PICTURE'],
				array('width'=>969, 'height'=>969),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true,
				100
			);?>
			<div id="popap<?=$arPop['ID'];?>" role="dialog" class="modal">
				<div class="container">
					<button data-dismiss="modal" class="close"></button>
					<h2><?=$arPop['NAME'];?></h2>
					<div class="module-body">
						<div class="img">
							<img src="<?=$imgProp['src'];?>">
							<div class="elements">
								<?foreach($arPop['CHILDREN'] as $arImg){
									$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"schems_items", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "schems_items",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "14",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PROPERTY_CODE" => array(
			0 => "LEFT",
			1 => "TOP",
			2 => "WIDTH",
			3 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => $arImg["ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_IBLOCK_CODE",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "blue",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N"
	),
	false
);
								}?>
							</div>
						</div>
						<div class="links">
							<ul>
								<?foreach($arPop['CHILDREN'] as $arLinks){?>
									<li>
										<a href="<?=$arLinks['LIST_PAGE_URL'];?><?=$arLinks['UF_IBLOCK_CODE']?>/" data-name="<?=$arLinks['CODE'];?>"><?=$arLinks['NAME'];?></a>
									</li>
								<?}?>
							</ul>
							<?if($arPop['PRICE']['SRC']){?>
								<a href="<?=$arPop['PRICE']['SRC'];?>" target="_blank" class="price">
									<span></span>
									<p>Прайс. <?=$arPop['NAME'];?></p>
								</a>
							<?}?>
						</div>
					</div>
				</div>
			</div>
		<?}?>
	</section>
<?}?>
