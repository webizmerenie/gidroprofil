<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

foreach($arResult['SECTIONS'] as $k => $arSection){
	$arResult['SECTIONS'][$k]['CATALOG_IMG'] = CFile::GetFileArray($arSection['UF_CATALOG_IMG']);
	$arResult['SECTIONS'][$k]['PRICE'] = CFile::GetFileArray($arSection['UF_PRICE']);
}

$newList = array();

foreach ($arResult['SECTIONS'] as $arItem1) {
	if ($arItem1['DEPTH_LEVEL'] != 1) continue;

	$arItem1['CHILDREN'] = array();

	foreach ($arResult['SECTIONS'] as $arItem2) {
		if ( $arItem2['DEPTH_LEVEL'] != 2 || $arItem2['IBLOCK_SECTION_ID'] != $arItem1['ID']) continue;

		$arItem2['CHILDREN'] = array();

		$arItem1['CHILDREN'][] = $arItem2;
	}

	$newList[] = $arItem1;
}
$arResult['SECTIONS'] = $newList;
