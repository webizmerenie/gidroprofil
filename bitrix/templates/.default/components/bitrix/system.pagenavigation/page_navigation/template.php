<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<?if ($arResult["NavShowAlways"] || $arResult["NavPageCount"] > 1){?>
<nav class="pagination-site">
	<ul>
			<li><a href="<?=$arResult['NAV']['URL']['PREV_PAGE']?>" class="prev"></a></li>
			<?foreach($arResult['NAV']['URL']['SOME_PAGE'] as $name => $href){?>
				<li>
					<?if($arResult['NAV']['PAGE_NUMBER'] != $name){?>
						<a href="<?=$href;?>"><?=$name;?></a>
					<?}else{?>
						<span class="active"><?=$name;?></span>
					<?}?>
				</li>
			<?}?>
			<li><a href="<?=$arResult['NAV']['URL']['NEXT_PAGE']?>" class="next"></a></li>
	</ul>
</nav>
<?}?>
