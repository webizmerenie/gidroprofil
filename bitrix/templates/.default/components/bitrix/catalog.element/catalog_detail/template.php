<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->AddChainItem($arResult['NAME'], $arResult['DETAIL_PAGE_URL']);
$img = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'],
			array('width'=>200, 'height'=>150),
			BX_RESIZE_IMAGE_PROPORTIONAL,
			true,
			100);?>
<section class="detail">
	<article>
		<div class="container">
			<h1><?=$arResult['IBLOCK_NAME'];?> / <?=$arResult['NAME'];?></h1>
			<div class="detail-table">
				<div class="col">
					<?if($img){?>
						<div class="img">
								<img src="<?=$img['src'];?>">
						</div>
					<?}?>
					<?if($arResult['PRICE']['CONVERT_PRICE']){?>
						<div class="price">
							<?=$arResult['PRICE']['CONVERT_PRICE'];?> руб.
						</div>
					<?}?>
					<button class="btn-inverse add-basket" data-id="<?=$arResult['ID'];?>">
						Добавить к заявке
					</button>
				</div>
				<div class="col">
					<div class="charset">
						<h4>ОСНОВНЫЕ ХАРАКТЕРИСТИКИ</h4>
						<ul>
							<?foreach($arResult['CHARSET'] as $arProp){?>
								<li>
									<p><?=$arProp['NAME'];?></p>
									<span><?=$arProp['VALUE'];?></span>
								</li>
							<?}?>
						</ul>
					</div>
					<div class="desc">
						<h4>Описание товара</h4>
						<?=$arResult['DETAIL_TEXT'];?>
					</div>
				</div>
				<div class="col">
					<?$APPLICATION->IncludeComponent(
						"bitrix:news.list",
						"contacts_list_catalog_detail",
						array(
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"ADD_SECTIONS_CHAIN" => "N",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
							"CHECK_DATES" => "Y",
							"COMPONENT_TEMPLATE" => "contacts_list_catalog_detail",
							"DETAIL_URL" => "",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"DISPLAY_DATE" => "Y",
							"DISPLAY_NAME" => "Y",
							"DISPLAY_PICTURE" => "Y",
							"DISPLAY_PREVIEW_TEXT" => "Y",
							"DISPLAY_TOP_PAGER" => "N",
							"FIELD_CODE" => array(
								0 => "",
								1 => "",
							),
							"FILTER_NAME" => "",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"IBLOCK_ID" => "13",
							"IBLOCK_TYPE" => "content",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"INCLUDE_SUBSECTIONS" => "N",
							"MESSAGE_404" => "",
							"NEWS_COUNT" => "2",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_TEMPLATE" => ".default",
							"PAGER_TITLE" => "Новости",
							"PARENT_SECTION" => "",
							"PARENT_SECTION_CODE" => "",
							"PREVIEW_TRUNCATE_LEN" => "",
							"PROPERTY_CODE" => array(
								0 => "EMAIL",
								1 => "TELEPHONE",
								2 => "",
							),
							"SET_BROWSER_TITLE" => "N",
							"SET_LAST_MODIFIED" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_STATUS_404" => "N",
							"SET_TITLE" => "N",
							"SHOW_404" => "N",
							"SORT_BY1" => "SORT",
							"SORT_BY2" => "NAME",
							"SORT_ORDER1" => "ASC",
							"SORT_ORDER2" => "DESC"
						),
						false
					);?>
					<div class="links-block">
						<ul>
							<?foreach($arResult['FILES'] as $arFile){?>
								<li>
									<?if($arFile['SRC']){?>
										<a href="<?=$arFile['SRC'];?>" download><span></span>
											<p><?=$arFile['NAME']?></p>
										</a>
									<?}?>
								</li>
							<?}?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?$APPLICATION->IncludeComponent("bitrix:news.list", "detail_form", Array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
				"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
				"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"FIELD_CODE" => array(	// Поля
					0 => "",
					1 => "",
				),
				"FILTER_NAME" => "",	// Фильтр
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
				"IBLOCK_ID" => "47",	// Код информационного блока
				"IBLOCK_TYPE" => "seo",	// Тип информационного блока (используется только для проверки)
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
				"NEWS_COUNT" => "20",	// Количество новостей на странице
				"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
				"PAGER_TITLE" => "Новости",	// Название категорий
				"PARENT_SECTION" => "",	// ID раздела
				"PARENT_SECTION_CODE" => "",	// Код раздела
				"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
				"PROPERTY_CODE" => array(	// Свойства
					0 => "",
					1 => "",
				),
				"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
				"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
				"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
				"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SHOW_404" => "N",	// Показ специальной страницы
				"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
				"SORT_BY2" => "ACTIVE_FROM",	// Поле для второй сортировки новостей
				"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
				"SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
				"ITEM_NAME" => $arResult['NAME']
			),
			false
		);?>
		<?if($arResult['SUBITEM']){?>
			<section class="subitem">
				<div class="container">
					<h3>Сопутствующие группы товаров</h3>
					<ul>
						<?foreach($arResult['SUBITEM'] as $arSubitem){
							$img = CFile::ResizeImageGet(
								$arSubitem['PICTURE'],
								array('width'=>212, 'height'=>212),
								BX_RESIZE_IMAGE_PROPORTIONAL,
								true,
								100
							);?>
							<li>
								<a href="<?=$arSubitem['DETAIL_PAGE_URL'];?>">
									<div class="img">
										<?if($img){?>
											<img src="<?=$img['src'];?>">
										<?}else{?>
											<span>ФОТО</span>
										<?}?>
									</div>
									<p class="name"><?=$arSubitem['NAME'];?></p></a>
								<p class="price"><?=$arSubitem['PRICE']['CONVERT_PRICE'];?> руб.</p>
								<button class="btn add-basket" data-id="<?=$arSubitem['ID'];?>">Добавить к заявке</button>
							</li>
						<?}?>
					</ul>
				</div>
			</section>
		<?}?>
	</article>
	<div class="modal-window">
		<button class="close" data-target=".modal-window"></button>
		<div class="text">
			<h3>Ваш товар добавлен к заявке.</h3>
			<a href="/moya-zayavka/">Перейти в мою заявку</a>
			<a href="#close" class="close" data-target=".modal-window">Продолжить выбор</a>
		</div>
	</div>
</section>
