<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/main/inc/convert_curs.php');

//get name inforblock
$iblock_name = CIBlock::GetArrayByID($arResult['IBLOCK_ID'], "NAME");
$arResult['IBLOCK_NAME'] = $iblock_name;

//vars
$charset = array();
$files = array();
$price = array();
$subitems = array();

//check block_type from price list
if($arResult['IBLOCK_TYPE_ID'] == "prom_podgotovka"){
	$id_anothre_block = 69;
}else{
	$id_anothre_block = 70;
}

//get propytes section where price lists
$price_another_block = CIBlockSection::GetList(
	array(),
	array('IBLOCK_ID' => 14,'ID' => $id_anothre_block),
	false,
	array("UF_PRICE"),
	array())->Fetch();

//add price lists from array
$files[] = CFile::GetFileArray($price_another_block['UF_PRICE']);
$files[0]['NAME'] = "Прайс. ".$price_another_block['NAME'];

//sorting array propytes
foreach($arResult['PROPERTIES'] as $arProp){
	if($arProp['PROPERTY_TYPE'] == "S"){
		$charset[] = $arProp;
	}else if($arProp['PROPERTY_TYPE'] == "F"){
		$arProp['SRC'] = CFile::GetPath($arProp['VALUE']);
		$files[] = $arProp;
	}else if($arProp['PROPERTY_TYPE'] == "N" && !empty($arProp['VALUE'])){
		$price = $arProp;
		$price['CONVERT_PRICE'] = "";
	}else if($arProp['PROPERTY_TYPE'] == "E"){
		foreach($arProp['VALUE'] as $arVals){
			$res = CIBlockElement::GetByID($arVals);
			if($ar_res = $res->GetNext()){
				$ar_res['PICTURE'] = CFile::GetFileArray($ar_res['DETAIL_PICTURE']);
				$property = CIBlockElement::GetProperty(
					$ar_res['IBLOCK_ID'],
					$ar_res['ID'],
					Array("sort"=>"asc"),
					Array("PROPERTY_TYPE" => "N", "EMPTY" => "N")
					)->Fetch();
				$ar_res['PRICE'] = $property;
				$subitems[] = $ar_res;
			}
		}
	}
}

//comvert curs
$price = convert($price, $arParams);
foreach ($subitems as $key => $subitem) {
	$subitems[$key]['PRICE'] = convert($price, $arParams);
}

//add sorting arrays
$arResult['CHARSET'] = $charset;
$arResult['FILES'] = $files;
$arResult['PRICE'] = $price;
$arResult['SUBITEM'] = $subitems;
