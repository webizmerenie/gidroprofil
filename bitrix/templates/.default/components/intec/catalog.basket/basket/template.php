<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult['ITEMS']){?>
	<section class="application-table">
		<div class="container">
			<table class="table table-hover">
				<thead>
					<tr>
						<td></td>
						<td>Фотография товара</td>
						<td>Наименование товара</td>
						<td>Количество</td>
						<td>Стоимость</td>
					</tr>
				</thead>
				<tbody>
					<?foreach($arResult['ITEMS'] as $arItem){
						$img = CFile::ResizeImageGet(
							$arItem['DETAIL_PICTURE'],
							array('width'=>212, 'height'=>212),
							BX_RESIZE_IMAGE_PROPORTIONAL,
							true,
							100
						);?>
						<tr>
							<td>
								<a href="#delete" title="удалить" class="delete" data-id="<?=$arItem['ID'];?>"></a>
							</td>
							<td>
								<div class="img">
									<a href="<?=$arItem['DETAIL_PAGE_URL'];?>">
										<?if($img){?>
											<img src="<?=$img['src'];?>">
										<?}else{?>
											<span>фото</span>
										<?}?>
									</a>
								</div>
							</td>
							<td>
								<div class="name">
									<a href="<?=$arItem['DETAIL_PAGE_URL'];?>">
										<?=$arItem['NAME'];?>
									</a>
								</div>
							</td>
							<td>
								<input type="text" name="count" maxlength="3" value="<?=$arItem['COUNT'];?>" class="count" data-id="<?=$arItem['ID'];?>">
								<a href="#add" class="add">+</a>
							</td>
							<td><span class="item-price"><?=$arItem['TOTAL_PRICE'];?></span> руб.</td>
						</tr>
					<?}?>
					<tr class="total">
						<td colspan="3">Общая сумма:</td>
						<td class="total-count"><span><?=$arResult['TOTAL_COUNT'];?></span> шт.</td>
						<td class="total-price"><span><?=$arResult['TOTAL_PRICE'];?></span> руб.
							<div class="tooltip-custom">
								<button class="icon-tooltip"></button>
								<div class="toolpti-text sm-left">За дополнительной скидкой обратитесь к вашему менеджеру</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</section>
	<?$APPLICATION->IncludeComponent("bitrix:news.list", "application_form", Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
			"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
			"AJAX_MODE" => "N",	// Включить режим AJAX
			"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
			"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
			"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
			"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
			"CACHE_GROUPS" => "Y",	// Учитывать права доступа
			"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
			"CACHE_TYPE" => "A",	// Тип кеширования
			"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
			"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
			"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
			"DISPLAY_DATE" => "Y",	// Выводить дату элемента
			"DISPLAY_NAME" => "Y",	// Выводить название элемента
			"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
			"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
			"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
			"FIELD_CODE" => array(	// Поля
				0 => "",
				1 => "",
			),
			"FILTER_NAME" => "",	// Фильтр
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
			"IBLOCK_ID" => "47",	// Код информационного блока
			"IBLOCK_TYPE" => "seo",	// Тип информационного блока (используется только для проверки)
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
			"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
			"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
			"NEWS_COUNT" => "20",	// Количество новостей на странице
			"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
			"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
			"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
			"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
			"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
			"PAGER_TITLE" => "Новости",	// Название категорий
			"PARENT_SECTION" => "",	// ID раздела
			"PARENT_SECTION_CODE" => "",	// Код раздела
			"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
			"PROPERTY_CODE" => array(	// Свойства
				0 => "",
				1 => "",
			),
			"SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
			"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
			"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
			"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
			"SET_STATUS_404" => "N",	// Устанавливать статус 404
			"SET_TITLE" => "N",	// Устанавливать заголовок страницы
			"SHOW_404" => "N",	// Показ специальной страницы
			"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
			"SORT_BY2" => "NAME",	// Поле для второй сортировки новостей
			"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
			"SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
		),
		false
	);?>
<?}else{?>
	<div class="container">
		<section class="empty">
			<p>Ваша корзина пока пуста. <br> Купить товары вы можете в нашем
			<a href="/katalog/">каталоге</a></p>
		</section>
	</div>
<?}?>
