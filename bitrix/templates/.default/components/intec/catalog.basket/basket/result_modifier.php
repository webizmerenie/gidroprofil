<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arResult = $_SESSION['CART'];
$arResult['TOTAL_PRICE'] = 0;

foreach($arResult['ITEMS'] as $item){
	$arResult['TOTAL_PRICE'] += $item['TOTAL_PRICE'];
}
