<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/katalog/\".\$id_ib[\"IBLOCK_CODE\"].\"/filters/(.+?)/apply/\\??(.*)#",
		"RULE" => "SMART_FILTER_PATH=\$1&\$2",
		"ID" => "bitrix:catalog.smart.filter",
		"PATH" => "/katalog/section.php",
	),
	array(
		"CONDITION" => "#^/katalog/(.+?)/filters/(.+?)/apply/\\??(.*)#",
		"RULE" => "IBLOCK_ID=\$1&SMART_FILTER_PATH=\$2&\$3",
		"ID" => "",
		"PATH" => "/katalog/section.php",
	),
	array(
		"CONDITION" => "#^/katalog/(.+?)/(.+?).php(\\?|\$)#",
		"RULE" => "IBLOCK_ID=\$1&ELEMENT_CODE=\$2&",
		"ID" => "",
		"PATH" => "/katalog/detail.php",
	),
	array(
		"CONDITION" => "#^/katalog/(.+?)/(\\?|\$)#",
		"RULE" => "IBLOCK_ID=\$1&",
		"ID" => "",
		"PATH" => "/katalog/section.php",
	),
);

?>