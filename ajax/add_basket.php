<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/main/inc/convert_curs.php');
CModule::IncludeModule("iblock");

session_start();
function updateTotal(){
	$_SESSION['CART']['TOTAL_COUNT'] += $_POST['count'];
}

function addItem(){
	$res = CIBlockElement::GetByID($_POST['id']);
	if($ar_res = $res->GetNextElement()){
		$fields = $ar_res->GetFields();
		$props = $ar_res->GetProperties(
			array(),
			array("EMPTY"=>"N", "PROPERTY_TYPE"=>"N")
		);

		$items = $fields;
		foreach($props as $prop){
			$items['PRICE'] = convert($prop);
		}

		$items['DETAIL_PICTURE'] = CFile::GetFileArray($items['DETAIL_PICTURE']);
		$items['COUNT'] = $_POST['count'];
		$items['TOTAL_PRICE'] = $items['PRICE']['CONVERT_PRICE'] * $items['COUNT'];
	}
	return $items;
}

if(!isset($_SESSION['CART'])){
	$_SESSION['CART'] = array();
	$_SESSION['CART']['TOTAL_COUNT'] = 0;
	$_SESSION['CART']['TOTAL_PRICE'] = 0;
	$_SESSION['CART']['ITEMS'][] = addItem();
	updateTotal();
}else{
	$index = NULL;

	foreach($_SESSION['CART']['ITEMS'] as $k => $cart){
		if($cart['ID'] == $_POST['id']){
			$index = $k;
		}
	}

	if(is_null($index)){
		$_SESSION['CART']['ITEMS'][] = addItem();
		updateTotal();
	}else{
		$_SESSION['CART']['ITEMS'][$index]['COUNT'] += $_POST['count'];
		$_SESSION['CART']['ITEMS'][$index]['TOTAL_PRICE'] = $_SESSION['CART']['ITEMS'][$index]['COUNT'] * $_SESSION['CART']['ITEMS'][$index]['PRICE']['CONVERT_PRICE'];
		updateTotal();
	}
}
header('Content-Type: application/json');
print json_encode($_SESSION['CART']);
