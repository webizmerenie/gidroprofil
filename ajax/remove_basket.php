<?
session_start();
foreach($_SESSION['CART']['ITEMS'] as $k => $item){
	if($_POST['id'] == $item['ID']){
		unset($_SESSION['CART']['ITEMS'][$k]);
	}
}

$_SESSION['CART']['TOTAL_COUNT'] = 0;
$_SESSION['CART']['TOTAL_PRICE'] = 0;

foreach($_SESSION['CART']['ITEMS'] as $item){
	$_SESSION['CART']['TOTAL_COUNT'] += $item['COUNT'];
	$_SESSION['CART']['TOTAL_PRICE'] += $item['TOTAL_PRICE'];
}
header('Content-Type: application/json');
print json_encode($_SESSION['CART']);
