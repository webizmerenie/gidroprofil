<?
//init depends
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/PHPMailer/PHPMailerAutoload.php');
session_start();

//include classes
CModule::IncludeModule("iblock");
$mail = new PHPMailer();

//init vars
$email = CIBlockElement::GetByID($_POST['region'])->Fetch();
$text = "";
$subject = "";
$section_ib = "";

if($_POST['type_form'] == 'application'){
	$totalPrice = 0;
	$section_ib = 287;
	$subject = "Заявка с сайта ".$_SERVER['HTTP_HOST']." от ".$_POST['name']."";

	$text .= "
		<table>
			<thead>
				<th>Название</th>
				<th>Количество</th>
				<th>Стоимость</th>
			</thead>
		<tbody>";

	foreach($_SESSION['CART']['ITEMS'] as $item){
		$items = "
			<tr>
				<td>".$item['NAME']."</td>
				<td style='text-align:center;'>".$item['COUNT']." шт.</td>
				<td style='text-align:center;'>".$item['TOTAL_PRICE']." руб.</td>
			</tr>";

		$totalPrice += $item['TOTAL_PRICE'];
		$text .= $items;
	}

	$text .= "
		<tr style='font-weight:bold;'>
			<td>Общая сумма:</td>
			<td style='text-align:center;'>".$_SESSION['CART']['TOTAL_COUNT']." шт.</td>
			<td style='text-align:center;'>".$totalPrice." руб.</td>
		</tr>
	";
	$text .= "</tbody></table>";
}

if($_POST['type_form'] == 'detail_page'){
	$section_ib = 294;
	$subject = "Вопрос с сайта ".$_SERVER['HTTP_HOST']." по товару ".$_POST['item_name']."";

	$total_size_file = 0;

	foreach ($_FILES as $k => $file) {
		$total_size_file += $file['size'];
	}
}

if($_POST['type_form'] == 'call_me'){
	$section_ib = 295;
	$subject = "Вопрос с сайта ".$_SERVER['HTTP_HOST'];
}

$text .= "
	<ul>
		<li>Имя - ".$_POST['name']."</li>
		<li>Телефон или email - ".$_POST['tel']."</li>
	</ul>
	<b>Сообщение:</b>
	<p>".$_POST['msg']."</p>";

$mail->isSMTP();
$mail->SMTPAuth = true;
require_once($_SERVER['DOCUMENT_ROOT'].'/ajax/smtp_config.php');
$mail->AddAddress($email['CODE']);
$mail->CharSet = "UTF-8";
$mail->isHTML(true);
$mail->Subject = $subject;
$mail->Body = $text;


if($_POST['type_form'] == 'detail_page'){
	if($total_size_file < 10485760){
		$PROP = array();
		$PROP[507] = array();

		foreach ($_FILES as $k => $file) {
			if($file['name']){
				$allowed_filetypes = array('.png', '.jpg', '.jpeg', '.gif', '.tif', '.xls',
					'.doc', '.docx', '.odt', '.pdf');
				$ext = substr($file['name'], strripos($file['name'],'.'), strlen($file['name'])-1);

				if(in_array($ext,$allowed_filetypes)){
					$mail->AddAttachment($file['tmp_name'] , $file['name']);
					$PROP[507]['n'.$k] = $file;
				}
			}
		}
	}
}

//add iblock bitrix
$arFileds = array(
		"MODIFIED_BY" => $USER->GetID(),
		"IBLOCK_ID" => 48,
		"IBLOCK_SECTION_ID" => $section_ib,
		"NAME" => $subject,
		"ACTIVE" => "N",
		"DETAIL_TEXT" => $text,
		"PROPERTY_VALUES" => $PROP
	);

$element = new CIBlockElement;
$newElement = $element->Add($arFileds);

if(!$mail->send()) {
	echo 'Mailer Error: ' . $mail->ErrorInfo;
}else{
	if($_POST['type_form'] == 'application'){
		unset($_SESSION['CART']);
	}
}
