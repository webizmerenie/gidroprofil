<?
session_start();
$result = array();

foreach($_SESSION['CART']['ITEMS'] as $k => $item){
	if($_POST['id'] == $item['ID']){
		$_SESSION['CART']['ITEMS'][$k]['COUNT'] = $_POST['count'];
		$_SESSION['CART']['ITEMS'][$k]['TOTAL_PRICE'] = $item['PRICE']['CONVERT_PRICE'] * $_POST['count'];
	}
}

$_SESSION['CART']['TOTAL_COUNT'] = 0;
$_SESSION['CART']['TOTAL_PRICE'] = 0;

foreach($_SESSION['CART']['ITEMS'] as $item){
	if($_POST['id'] == $item['ID']){
		$result['ITEM_PRICE'] = $item['TOTAL_PRICE'];
	}
	$_SESSION['CART']['TOTAL_COUNT'] += $item['COUNT'];
	$_SESSION['CART']['TOTAL_PRICE'] += $item['TOTAL_PRICE'];
}

$result['TOTAL_COUNT'] = $_SESSION['CART']['TOTAL_COUNT'];
$result['TOTAL_PRICE'] = $_SESSION['CART']['TOTAL_PRICE'];

header('Content-Type: application/json');
print json_encode($result);
