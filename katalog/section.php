<?
define('NO_CONTAINER', 'Y');
define('NO_TITLE', 'Y');
define('CATALOG', 'Y');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetTitle("");?>
<?$id_ib = $APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath('inc/get_id_iblock.php'),
	Array(),
	Array()
);?>
<section class="list-items">
	<section class="title-img">
		<?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"breadcrumb",
			array(
				"COMPONENT_TEMPLATE" => "breadcrumb",
				"PATH" => "",
				"SITE_ID" => "s1",
				"START_FROM" => "0"
			),
			false
		);?>
		<img src="<?=$id_ib['IMG']['src'];?>">
	</section>
	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.smart.filter",
		"filter_horizontal",
		array(
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "N",
			"DISPLAY_ELEMENT_COUNT" => "Y",
			"FILTER_NAME" => "arrFilter",
			"FILTER_VIEW_MODE" => "horizontal",
			"IBLOCK_ID" => $id_ib["IBLOCK_ID"],
			"IBLOCK_TYPE" => "",
			"INSTANT_RELOAD" => "Y",
			"PAGER_PARAMS_NAME" => "arrPager",
			"POPUP_POSITION" => "left",
			"SAVE_IN_SESSION" => "N",
			"SECTION_CODE" => "",
			"SECTION_CODE_PATH" => "",
			"SECTION_DESCRIPTION" => "-",
			"SECTION_ID" => "",
			"SECTION_TITLE" => "-",
			"SEF_MODE" => "Y",
			"SEF_RULE" => "/katalog/".$id_ib["IBLOCK_CODE"]."/filters/#SMART_FILTER_PATH#/apply/",
			"SMART_FILTER_PATH" => $_REQUEST["SMART_FILTER_PATH"],
			"TEMPLATE_THEME" => "blue",
			"XML_EXPORT" => "N",
			"COMPONENT_TEMPLATE" => "filter_horizontal"
		),
		false
	);?>

	<section class="items">
		<div class="container">
			<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"catalog_section", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/moya-zayavka/",
		"BROWSER_TITLE" => "NAME",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "86400",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "catalog_section",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => $id_ib["IBLOCK_ID"],
		"IBLOCK_TYPE" => "bytovaya_vodopodgotovka",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "5",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "page_navigation",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "10",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
		),
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "count",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "all",
			2 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_CODE_PATH" => "",
		"SECTION_ID" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SEF_RULE" => "/katalog/",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "blue",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"USD" => $_SESSION["CURS"]["DOLAR"],
		"EUR" => $_SESSION["CURS"]["EUR"]
	),
	false
);?>
		</div>
	</section>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
